<?php

namespace Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_recurring\BillingPeriod;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\commerce_recurring\Plugin\Commerce\UsageType\UsageTypeInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Utility methods for tracking and retrieving usage.
 *
 * This trait is recommended for subscriptions that implement
 * SupportsUsageInterface. It simplifies interacting with usage type plugins by
 * "proxying" them from the subscription type instead of requiring direct
 * plugin instantiation. That is, of course, still an option if your use case
 * requires it, but most implementations will find these methods useful.
 */
trait UsageHelperTrait {

  /**
   * Record usage of a given type for a given period.
   *
   * @param \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription
   *   The subscription to which to add the usage.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $usage_variation
   *   The variation to be used when calculating the cost of each usage.
   * @param int $quantity
   *   How many usages to add.
   * @param \Drupal\commerce_recurring\BillingPeriod $period
   *   The billing period to which to add the usage. The default is the current
   *   billing period.
   * @param string $usage_type
   *   The ID of the plugin providing usage tracking. The plugin must implement
   *   \Drupal\commerce_recurring\Plugin\Commerce\UsageType\UsageTypeInterface.
   */
  public function addUsage(SubscriptionInterface $subscription, ProductVariationInterface $usage_variation, $quantity = 1, BillingPeriod $period = NULL, $usage_type = 'counter') {
    if (!isset($period)) {
      $period = new BillingPeriod(new DrupalDateTime(), new DrupalDateTime());
    }

    $instance = $this->loadUsageType($subscription, $usage_type);
    $instance->addUsage([
      'product_variation' => $usage_variation,
      'quantity' => $quantity,
    ], $period->getStartDate(), $period->getEndDate());
  }

  /**
   * Loads a plugin that can handle usage tracking.
   *
   * @param \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription
   *   The subscription to add the usage to.
   * @param string $usage_type
   *   The ID of the plugin providing usage tracking.
   *
   * @return \Drupal\commerce_recurring\Plugin\Commerce\UsageType\UsageTypeInterface
   *   The instantiated plugin.
   */
  public function loadUsageType(SubscriptionInterface $subscription, $usage_type = 'counter'): UsageTypeInterface {
    /** @var \Drupal\commerce_recurring\Plugin\Commerce\UsageType\UsageTypeInterface $instance */
    $instance = $this->usageTypeManager->createInstance($usage_type, ['subscription' => $subscription]);
    return $instance;
  }

  /**
   * Retrieve summarized usage for a particular billing period.
   *
   * @param \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription
   *   The subscription to check for usage.
   * @param \Drupal\commerce_recurring\BillingPeriod $period
   *   The billing period to check for usage.
   *
   * @return array
   *   A multi-dimensional array containing summarized usage records
   *   for the given period. The array structure is:
   *
   *   First level: Usage type plugin ID.
   *   Second level: Product variation ID (representing the cost of the usage).
   *   Third level: Quantity.
   *
   *   Example:
   *
   *   'counter' => [
   *     1 => 150,
   *     4 => 200,
   *   ],
   *
   *   In this example, 150 usages of the product variation with ID 1 and 200
   *   usages of the product variation with ID 4 have been recorded. They are
   *   using the 'counter' plugin.
   *
   *   You cannot necessarily rely on this data for invoice totals. If you have
   *   to calculate or re-calculate billing for some reason, you should use
   *   plugin-specific methods.
   */
  public function getUsageForPeriod(SubscriptionInterface $subscription, BillingPeriod $period) {
    $usage_records = [];
    $usage_types = $this->usageTypeManager->getDefinitions();
    foreach ($usage_types as $id => $definition) {
      /** @var \Drupal\commerce_recurring\Plugin\Commerce\UsageType\UsageTypeInterface $usage_type */
      $usage_type = $this->usageTypeManager->createInstance($id, ['subscription' => $subscription]);
      $usage_records[$id] = $usage_type->usageHistory($period);
    }

    return $usage_records;
  }

}
