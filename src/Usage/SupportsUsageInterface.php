<?php

namespace Drupal\commerce_recurring\Usage;

/**
 * This interface declares a subscription type as supporting usage tracking.
 *
 * You must implement this interface if you want usage added by your
 * subscription type to be invoiced in recurring orders.
 */
interface SupportsUsageInterface {

}
