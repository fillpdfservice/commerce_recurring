<?php

namespace Drupal\commerce_recurring_test\Plugin\Commerce\SubscriptionType;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_recurring\Entity\SubscriptionInterface;
use Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\ProductVariation;
use Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\UsageHelperTrait;
use Drupal\commerce_recurring\Usage\SubscriptionFreeUsageInterface;
use Drupal\commerce_recurring\Usage\SupportsUsageInterface;

/**
 * Test class for usage tracking against a product variation.
 *
 * @CommerceSubscriptionType(
 *  id = "usage_test_product_variation",
 *  label = @Translation("Usage test product variation."),
 * )
 */
class UsageTestProductVariation extends ProductVariation implements SupportsUsageInterface, SubscriptionFreeUsageInterface {

  use UsageHelperTrait;

  /**
   * {@inheritdoc}
   */
  public function getFreeQuantity(ProductVariationInterface $variation, SubscriptionInterface $subscription) {
    if ($variation->getSku() === 'variation_300_free') {
      return 300;
    }

    if ($variation->getSku() === 'variation_5_free') {
      return 5;
    }

    return 0;
  }

}
