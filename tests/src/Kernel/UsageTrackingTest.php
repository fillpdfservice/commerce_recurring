<?php

namespace Drupal\Tests\commerce_recurring\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_recurring\BillingPeriod;
use Drupal\commerce_recurring\Entity\BillingSchedule;
use Drupal\commerce_recurring\Entity\Subscription;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Tests usage tracking functionality, as well as the Counter and Gauge plugins.
 *
 * @group commerce_recurring
 */
class UsageTrackingTest extends RecurringKernelTestBase {

  public static $modules = ['commerce_recurring_test'];

  /**
   * The recurring order manager.
   *
   * @var \Drupal\commerce_recurring\RecurringOrderManagerInterface
   */
  protected $recurringOrderManager;

  /**
   * A product variation to be used for tracking counter usage.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariation
   */
  protected $counterVariation;

  /**
   * A product variation to be used for tracking gauge usage.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariation
   */
  protected $gaugeVariation;

  /**
   * A product variation that creates a subscription with usage tracking.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariation
   */
  protected $usageSubscriptionVariation;

  /**
   * A prepaid billing schedule.
   *
   * @var \Drupal\commerce_recurring\Entity\BillingScheduleInterface
   */
  protected $prepaidBillingSchedule;

  /**
   * A prepaid subscription that supports usage tracking.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $prepaidUsageSubscription;

  /**
   * Test usage tracking with the 'counter' plugin.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function testCounterUsageTracking() {
    //
    //
    // COMMON
    //
    // .
    $counter_variation = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '0.05',
        'currency_code' => 'USD',
      ],
    ]);
    $counter_variation->save();
    $this->counterVariation = $this->reloadEntity($counter_variation);

    $cv_free_quantity = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => 'variation_300_free',
      'price' => [
        'number' => '0.05',
        'currency_code' => 'USD',
      ],
    ]);
    $cv_free_quantity->save();

    //
    //
    // POSTPAID
    //
    // .
    $added_subscription = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->usageSubscriptionVariation,
      'unit_price' => [
        'number' => '0.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '1',
    ]);
    $added_subscription->save();
    $initial_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$added_subscription],
      'state' => 'draft',
      'payment_method' => $this->paymentMethod,
    ]);
    $initial_order->save();

    $workflow = $initial_order->getState()->getWorkflow();
    $initial_order->getState()
      ->applyTransition($workflow->getTransition('place'));
    $initial_order->save();

    $subscriptions = Subscription::loadMultiple();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
    $subscription = reset($subscriptions);

    // Increase counter usage by 1.
    /** @var \Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\UsageHelperTrait $subscription_type */
    $subscription_type = $subscription->getType();
    $subscription_type->addUsage($subscription, $counter_variation);

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->recurringOrderManager->ensureOrder($subscription);

    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $field_billing_period */
    $field_billing_period = $order->get('billing_period')->first();
    $usage = $subscription_type->getUsageForPeriod($subscription, $field_billing_period->toBillingPeriod());
    $latest_usage = end($usage['counter']);
    self::assertEquals($latest_usage->getQuantity(), 1, 'One counter usage was recorded.');

    $items = $order->getItems();
    self::assertCount(2, $items, 'Order has two items (subscription and added usage).');

    // Make it 300 total.
    $subscription_type->addUsage($subscription, $counter_variation, 299);
    $this->recurringOrderManager->refreshOrder($order);

    $items = $order->getItems();
    self::assertCount(2, $items, 'Order still has two items (subscription and combined usage).');

    /** @var \Drupal\commerce_order\Entity\OrderItem $usage_item */
    $usage_item = end($items);
    self::assertEquals(new Price('15', 'USD'), $usage_item->getTotalPrice(), 'Usage total is 15 USD.');
    self::assertEquals(new Price('15', 'USD'), $order->getTotalPrice(), 'Order total is 15 USD.');

    // Add 301 of the counter variation with 300 free. This should increase the
    // price by 0.05.
    $subscription_type->addUsage($subscription, $cv_free_quantity, 301);
    $this->recurringOrderManager->refreshOrder($order);
    self::assertEquals(new Price('15.05', 'USD'), $order->getTotalPrice(), 'Counter usage type: Free quantity works.');

    //
    //
    // PREPAID
    //
    // .
    $added_prepaid_subscription = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->prepaidUsageSubscription,
      'unit_price' => [
        'number' => '5.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '1',
    ]);
    $added_prepaid_subscription->save();
    $prepaid_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$added_prepaid_subscription],
      'state' => 'draft',
      'payment_method' => $this->paymentMethod,
    ]);
    $prepaid_order->save();

    $prepaid_workflow = $prepaid_order->getState()->getWorkflow();
    $prepaid_order->getState()
      ->applyTransition($prepaid_workflow->getTransition('place'));
    $prepaid_order->save();

    $subscriptions = Subscription::loadMultiple();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $prepaid_subscription */
    $prepaid_subscription = end($subscriptions);

    // Increase counter usage by 1.
    /** @var \Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\UsageHelperTrait $prepaid_subscription_type */
    $prepaid_subscription_type = $prepaid_subscription->getType();
    $prepaid_subscription_type->addUsage($prepaid_subscription, $counter_variation);

    /** @var \Drupal\commerce_order\Entity\Order $porder */
    $porder = $this->recurringOrderManager->ensureOrder($prepaid_subscription);

    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $pfield_billing_period */
    $pfield_billing_period = $porder->get('billing_period')->first();
    $pusage = $prepaid_subscription_type->getUsageForPeriod($prepaid_subscription, $pfield_billing_period->toBillingPeriod());
    $platest_usage = end($pusage['counter']);
    self::assertEquals($platest_usage->getQuantity(), 1, 'One counter usage was recorded.');

    $items = $porder->getItems();
    self::assertCount(2, $items, 'Order has two items (subscription and added usage).');

    // Make it 300 total.
    $prepaid_subscription_type->addUsage($prepaid_subscription, $counter_variation, 299);
    $this->recurringOrderManager->refreshOrder($porder);

    $items = $porder->getItems();
    self::assertCount(2, $items, 'Order still has two items (subscription and combined usage).');

    /** @var \Drupal\commerce_order\Entity\OrderItem $usage_item */
    $usage_item = end($items);
    self::assertEquals($usage_item->getTotalPrice(), new Price('15', 'USD'), 'Usage total is 15 USD.');
    self::assertEquals(new Price('20', 'USD'), $porder->getTotalPrice(), 'Order total is 20 USD.');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp() {
    parent::setUp();
    $this->installSchema('commerce_recurring', 'commerce_recurring_usage');

    $this->recurringOrderManager = $this->container->get('commerce_recurring.order_manager');

    /** @var \Drupal\commerce_recurring\Entity\BillingScheduleInterface $prepaid_billing_schedule */
    $prepaid_billing_schedule = BillingSchedule::create([
      'id' => 'prepaid_test_id',
      'label' => 'Hourly schedule',
      'displayLabel' => 'Hourly schedule',
      'billingType' => BillingSchedule::BILLING_TYPE_PREPAID,
      'plugin' => 'rolling',
      'configuration' => [
        'interval' => [
          'number' => '1',
          'unit' => 'hour',
        ],
      ],
    ]);
    $prepaid_billing_schedule->save();
    $this->prepaidBillingSchedule = $this->reloadEntity($prepaid_billing_schedule);

    // Postpaid subscription variation.
    $usage_subscription_variation = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '0.00',
        'currency_code' => 'USD',
      ],
      'billing_schedule' => $this->billingSchedule,
      'subscription_type' => [
        'target_plugin_id' => 'usage_test_product_variation',
      ],
    ]);
    $usage_subscription_variation->save();
    $this->usageSubscriptionVariation = $this->reloadEntity($usage_subscription_variation);

    // Prepaid subscription variation.
    $prepaid_subscription = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '5.00',
        'currency_code' => 'USD',
      ],
      'billing_schedule' => $this->prepaidBillingSchedule,
      'subscription_type' => [
        'target_plugin_id' => 'usage_test_product_variation',
      ],
    ]);
    $prepaid_subscription->save();
    $this->prepaidUsageSubscription = $this->reloadEntity($prepaid_subscription);
  }

  /**
   * Test usage tracking with the 'gauge' plugin.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function testGaugeUsageTracking() {
    //
    //
    // COMMON
    //
    // .
    $gauge_variation = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '5',
        'currency_code' => 'USD',
      ],
    ]);
    $gauge_variation->save();
    $this->gaugeVariation = $this->reloadEntity($gauge_variation);

    $gv_free_quantity = ProductVariation::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'sku' => 'variation_5_free',
      'price' => [
        'number' => '5',
        'currency_code' => 'USD',
      ],
    ]);
    $gv_free_quantity->save();

    //
    //
    // POSTPAID
    //
    // .
    // For sanity, mock the current time as the start of the hour.
    $fake_now = new \DateTime();
    $fake_now->setTime($fake_now->format('G'), 0);
    $this->rewindTime($fake_now->format('U'));
    $added_subscription = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->usageSubscriptionVariation,
      'unit_price' => [
        'number' => '0.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '1',
    ]);
    $added_subscription->save();
    $initial_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$added_subscription],
      'state' => 'draft',
      'payment_method' => $this->paymentMethod,
    ]);
    $initial_order->save();

    $workflow = $initial_order->getState()->getWorkflow();
    $initial_order->getState()
      ->applyTransition($workflow->getTransition('place'));
    $initial_order->save();

    $subscriptions = Subscription::loadMultiple();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
    $subscription = reset($subscriptions);
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $this->recurringOrderManager->ensureOrder($subscription);
    $field_billing_period = $order->get('billing_period')->first();
    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $field_billing_period */
    $order_period = $field_billing_period->toBillingPeriod();

    // Increase gauge usage by 4. Add it for the entire billing period.
    /** @var \Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\UsageHelperTrait $subscription_type */
    $subscription_type = $subscription->getType();
    $subscription_type->addUsage($subscription, $gauge_variation, 4, $order_period, 'gauge');

    $usage = $subscription_type->getUsageForPeriod($subscription, $order_period);
    $latest_usage = end($usage['gauge']);
    self::assertEquals($latest_usage->getQuantity(), 4, 'Four gauge usages were recorded.');

    $this->recurringOrderManager->refreshOrder($order);
    $items = $order->getItems();
    self::assertCount(2, $items, 'Order has two items (subscription and added usage).');

    // Check that overlaps of the same variation get resolved.
    // \DateTimePlus::add() was not adding, so converting back and forth.
    $halfway = $order_period->getStartDate()
      ->getPhpDateTime()
      ->add(new \DateInterval('PT30M'));
    $half_period = new BillingPeriod($order_period->getStartDate(), DrupalDateTime::createFromDateTime($halfway));
    $subscription_type->addUsage($subscription, $gauge_variation, 4, $half_period, 'gauge');
    $this->recurringOrderManager->refreshOrder($order);

    $items = $order->getItems();
    self::assertCount(3, $items, 'Order has three items (subscription and new usage).');
    $usage = $subscription_type->getUsageForPeriod($subscription, $order_period);
    $latest_usage = end($usage['gauge']);
    self::assertEquals($latest_usage->getQuantity(), 4, 'Four gauge usages were recorded.');

    /** @var \Drupal\commerce_order\Entity\OrderItem $usage_item */
    self::assertEquals(new Price('20', 'USD'), $order->getTotalPrice(), 'Order total is 20 USD.');

    // Test free quantity for gauge usage type.
    $subscription_type->addUsage($subscription, $gv_free_quantity, 7, $order_period, 'gauge');
    $this->recurringOrderManager->refreshOrder($order);
    // With free quantity applied, the second usage records comes out to two
    // usages at $5/period; $20 + $10 = $30.
    self::assertEquals(new Price('30', 'USD'), $order->getTotalPrice(), 'Gauge usage type: Free quantity works.');

    //
    //
    // PREPAID
    //
    // .
    $added_prepaid_subscription = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $this->prepaidUsageSubscription,
      'unit_price' => [
        'number' => '5.00',
        'currency_code' => 'USD',
      ],
      'quantity' => '1',
    ]);
    $added_prepaid_subscription->save();
    $prepaid_order = Order::create([
      'type' => 'default',
      'store_id' => $this->store,
      'uid' => $this->user,
      'order_items' => [$added_prepaid_subscription],
      'state' => 'draft',
      'payment_method' => $this->paymentMethod,
    ]);
    $prepaid_order->save();

    $prepaid_workflow = $prepaid_order->getState()->getWorkflow();
    $prepaid_order->getState()
      ->applyTransition($prepaid_workflow->getTransition('place'));
    $prepaid_order->save();

    $subscriptions = Subscription::loadMultiple();

    /** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $prepaid_subscription */
    $prepaid_subscription = end($subscriptions);
    /** @var \Drupal\commerce_order\Entity\Order $porder */
    $porder = $this->recurringOrderManager->ensureOrder($prepaid_subscription);
    /** @var \Drupal\commerce_recurring\Plugin\Field\FieldType\BillingPeriodItem $pfield_billing_period */
    $pfield_billing_period = $porder->get('billing_period')->first();
    $porder_period = $pfield_billing_period->toBillingPeriod();

    // Increase gauge usage by 4. Add it for the entire billing period.
    /** @var \Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\UsageHelperTrait $prepaid_subscription_type */
    $prepaid_subscription_type = $prepaid_subscription->getType();
    $prepaid_subscription_type->addUsage($prepaid_subscription, $gauge_variation, 4, $order_period, 'gauge');

    $pusage = $prepaid_subscription_type->getUsageForPeriod($prepaid_subscription, $porder_period);
    $platest_usage = end($pusage['gauge']);
    self::assertEquals($platest_usage->getQuantity(), 4, 'Four gauge usages were recorded.');

    $this->recurringOrderManager->refreshOrder($porder);
    $items = $porder->getItems();
    self::assertCount(2, $items, 'Order has two items (subscription and added usage).');

    // Check that overlaps of the same variation get resolved.
    // \DateTimePlus::add() was not adding, so converting back and forth.
    $halfway = $porder_period->getStartDate()
      ->getPhpDateTime()
      ->add(new \DateInterval('PT30M'));
    $half_period = new BillingPeriod($porder_period->getStartDate(), DrupalDateTime::createFromDateTime($halfway));
    $prepaid_subscription_type->addUsage($prepaid_subscription, $gauge_variation, 4, $half_period, 'gauge');
    $this->recurringOrderManager->refreshOrder($porder);

    $items = $porder->getItems();
    self::assertCount(3, $items, 'Order has three items (subscription and new usage).');
    $usage = $prepaid_subscription_type->getUsageForPeriod($prepaid_subscription, $porder_period);
    $latest_usage = end($usage['gauge']);
    self::assertEquals($latest_usage->getQuantity(), 4, 'Four gauge usages were recorded.');

    /** @var \Drupal\commerce_order\Entity\OrderItem $usage_item */
    self::assertEquals(new Price('25', 'USD'), $porder->getTotalPrice(), 'Order total is 20 USD.');

    // Test free quantity for gauge usage type.
    $prepaid_subscription_type->addUsage($prepaid_subscription, $gv_free_quantity, 7, $porder_period, 'gauge');
    $this->recurringOrderManager->refreshOrder($porder);
    // With free quantity applied, the second usage records comes out to two
    // usages at $5/period; $20 + $10 = $30.
    self::assertEquals(new Price('35', 'USD'), $porder->getTotalPrice(), 'Gauge usage type: Free quantity works.');
  }

}
