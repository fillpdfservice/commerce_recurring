<?php

/**
 * @file
 * Install, update and uninstall functions for the commerce_recurring module.
 */

use Drupal\Core\Database\Database;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_schema().
 */
function commerce_recurring_schema() {
  $schema['commerce_recurring_usage'] = [
    'description' => 'Tracks subscription usage.',
    'fields' => [
      'usage_id' => [
        'description' => 'The primary key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'usage_type' => [
        'description' => 'The ID of the usage type plugin providing tracking.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'subscription_id' => [
        'description' => 'The subscription ID.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'product_variation_id' => [
        'description' => 'The product variation ID for this record.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'quantity' => [
        'description' => 'The usage quantity.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'start' => [
        'description' => 'The Unix timestamp when usage began.',
        'type' => 'int',
        'not null' => FALSE,
      ],
      'end' => [
        'description' => 'The Unix timestamp when usage ended.',
        'type' => 'int',
        'not null' => FALSE,
      ],
    ],
    'primary key' => ['usage_id'],
    'indexes' => [
      'combined' => ['usage_type', 'subscription_id'],
      'timing' => ['start', 'end'],
    ],
  ];

  return $schema;
}

/**
 * Add the 'initial_order' field to 'commerce_subscription' entities.
 */
function commerce_recurring_update_8100(&$sandbox) {
  $storage_definition = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Initial order'))
    ->setDescription(t('The non-recurring order which started the subscription.'))
    ->setSetting('target_type', 'commerce_order')
    ->setSetting('handler', 'default')
    ->setSetting('display_description', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'entity_reference_autocomplete',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  $update_manager = \Drupal::entityDefinitionUpdateManager();
  $update_manager->installFieldStorageDefinition('initial_order', 'commerce_subscription', 'commerce_recurring', $storage_definition);
}

/**
 * Add the 'trial_starts' and "trial_ends" fields to subscriptions.
 */
function commerce_recurring_update_8101(&$sandbox) {
  $fields = [];
  $fields['trial_starts'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Trial starts'))
    ->setDescription(t('The time when the subscription trial starts.'))
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'timestamp',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'datetime_timestamp',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE);

  $fields['trial_ends'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Trial ends'))
    ->setDescription(t('The time when the subscription trial ends.'))
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'timestamp',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'datetime_timestamp',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE);

  $update_manager = \Drupal::entityDefinitionUpdateManager();
  foreach ($fields as $name => $storage_definition) {
    $update_manager->installFieldStorageDefinition($name, 'commerce_subscription', 'commerce_recurring', $storage_definition);
  }
}

/**
 * Make the billing_schedule field required on subscriptions.
 */
function commerce_recurring_update_8102() {
  $entity_definition_update = \Drupal::entityDefinitionUpdateManager();
  $field_definition = $entity_definition_update->getFieldStorageDefinition('billing_schedule', 'commerce_subscription');
  $field_definition->setRequired(TRUE);
  $entity_definition_update->updateFieldStorageDefinition($field_definition);
}

/**
 * Add the 'scheduled_changes' field to 'commerce_subscription' entities.
 */
function commerce_recurring_update_8103() {
  \Drupal::service('plugin.manager.field.field_type')->clearCachedDefinitions();

  $storage_definition = BaseFieldDefinition::create('commerce_scheduled_change')
    ->setLabel(t('Scheduled changes'))
    ->setRequired(FALSE)
    ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'commerce_scheduled_change_default',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', FALSE)
    ->setDisplayConfigurable('view', TRUE);

  $update_manager = \Drupal::entityDefinitionUpdateManager();
  $update_manager->installFieldStorageDefinition('scheduled_changes', 'commerce_subscription', 'commerce_recurring', $storage_definition);
}

/**
 * Add the 'commerce_usage_recurring' table.
 */
function commerce_recurring_update_8104() {
  $schema = Database::getConnection()->schema();
  if ($schema->tableExists('commerce_recurring_usage')) {
    return;
  }

  $spec = [
    'description' => 'Tracks subscription usage.',
    'fields' => [
      'usage_id' => [
        'description' => 'The primary key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'usage_type' => [
        'description' => 'The ID of the usage type plugin providing tracking.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'subscription_id' => [
        'description' => 'The subscription ID.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'product_variation_id' => [
        'description' => 'The product variation ID for this record.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'quantity' => [
        'description' => 'The usage quantity.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'start' => [
        'description' => 'The Unix timestamp when usage began.',
        'type' => 'int',
        'not null' => FALSE,
      ],
      'end' => [
        'description' => 'The Unix timestamp when usage ended.',
        'type' => 'int',
        'not null' => FALSE,
      ],
    ],
    'primary key' => ['usage_id'],
    'indexes' => [
      'combined' => ['usage_type', 'subscription_id'],
      'timing' => ['start', 'end'],
    ],
  ];

  $schema->createTable('commerce_recurring_usage', $spec);

  return "'commerce_recurring_usage' table installed.";
}
